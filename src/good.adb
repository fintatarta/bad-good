pragma Ada_2012;
package body Good with SPARK_Mode => On is

   -----------
   -- Index --
   -----------

   function Index
     (Source  : String;
      Pattern : String)
      return Natural
   is
      pragma Assert (Pattern'Length >= 1);
      pragma Assert (Source'First >= 1);
      function Pattern_At (K : Positive) return Boolean
        with Pre => K <= Source'Last - (Pattern'Length - 1) and K >= Source'First;

      function Pattern_At (K : Positive) return Boolean
      is (Source (K .. K+(Pattern'Length - 1)) = Pattern);
   begin
      if Source'Length < Pattern'Length then
         return 0;
      end if;

      for K in Source'First .. Source'Last - (Pattern'Length - 1) loop
         if  Pattern_At (K) then
            return K;
         end if;

         pragma Loop_Invariant (for all J in Source'First .. K => not Pattern_At (J));
      end loop;

      pragma Assert (for all J in Source'First .. Source'Last - (Pattern'Length - 1) => not Pattern_At (J));
      return 0;
   end Index;

end Good;
