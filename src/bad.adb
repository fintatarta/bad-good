pragma Ada_2012;
package body Bad with SPARK_Mode => On is

   -----------
   -- Index --
   -----------

   function Index
     (Source  : String;
      Pattern : String)
      return Natural
   is
      pragma Assert (Pattern'Length >= 1);
      pragma Assert (Source'First >= 1);
   begin
      if Source'Length < Pattern'Length then
         return 0;
      end if;

      for K in Source'First .. Source'Last - (Pattern'Length - 1) loop
         if  Source (K .. K+(Pattern'Length - 1)) = Pattern then
            return K;
         end if;

         pragma Loop_Invariant (for all J in Source'First .. K => Source (J .. J+(Pattern'Length - 1)) /= Pattern);
      end loop;

      pragma Assert (for all J in Source'First .. Source'Last - (Pattern'Length - 1) => Source (J .. J+(Pattern'Length - 1)) /= Pattern);
      return 0;
   end Index;

end Bad;
