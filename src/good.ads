package Good with SPARK_Mode => On is
   function Index (Source  : String;
                   Pattern : String)
                   return Natural
     with Pre => Pattern'Length > 0 and Source'Length > 0 and Source'First > 0;
end Good;
